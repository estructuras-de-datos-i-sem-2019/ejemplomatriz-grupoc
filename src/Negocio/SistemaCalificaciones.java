/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Modelo.Estudiante;
import Util.ExceptionUFPS;
import Util.LeerMatriz_Excel;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 *  Clase del negocio para la manipulación de una lista de Calificaciones
 * @author Marco Adarme
 */
public class SistemaCalificaciones {
    
    private Estudiante []listaEstructuras;

    public SistemaCalificaciones() {
    }
    
    
    public SistemaCalificaciones(String nombreArchivo) throws IOException {
        LeerMatriz_Excel miExcel=new LeerMatriz_Excel(nombreArchivo,0);
        String miMatriz[][]=miExcel.getMatriz();
        
        //Normalizar: Pasar de un archivo a un modelo de objetos
        this.listaEstructuras=new Estudiante[miMatriz.length-1];
        crearEstudiantes(miMatriz);
        
        
    }
    
    
    private void crearEstudiantes(String datos[][])
    {
    
        for(int fila=1;fila<datos.length;fila++)
        {
            //Datos para un estudiante
            String nombre="";
            long codigo=0;
            float quices[]=new float[datos[fila].length-2];
            int indice_quices=0;
            for(int columna=0;columna<datos[fila].length;columna++)
            {
            
                
                if(columna==0)
                    codigo=Long.parseLong(datos[fila][columna]);
                else
                {
                    if(columna==1)
                        nombre=datos[fila][columna];
                    else
                    {
                      try{
                        quices[indice_quices]=Float.parseFloat(datos[fila][columna]);
                        indice_quices++;
                        }catch(java.lang.NumberFormatException ex)
                      {
                            System.err.println("El dato ingresado para el quiz no es válido contiene un caracter");
                      }
                    }
                }
                
            }
            //Creó al estudiante
            Estudiante nuevo=new Estudiante(codigo, nombre);
            nuevo.setQuices(quices);
            //Ingresar al listado de Estudiantes:
            this.listaEstructuras[fila-1]=nuevo;
            
        
        }
          
        
    }
    
    
     public SistemaCalificaciones(int can) {
         
         // T identi[] = new T[tamaño];
         //T es Estudiante
            this.listaEstructuras=new Estudiante[can];
    }
    
    
     //Esto va a cambiar
     public void insertarEstudiante_EnPos0(long codigo,String nombre,  String notas) throws ExceptionUFPS
     {
         Estudiante nuevo=new Estudiante(codigo, nombre, notas);
         
         this.listaEstructuras[0]=nuevo;
         
     }

    @Override
    public String toString() {
        
        String msg="Mis estudiantes son:\n";
        
        for(Estudiante unEstudiante:this.listaEstructuras)
            msg+=unEstudiante.toString()+"\n";
        
        return msg;
        
    }
     
     
    /**
     * Obtiene los estudiantes cuyo promedio de quices es menor a 3
     * @return un vector de objetos de la clase Estudiante
     */
    public Estudiante[] getReprobaronQuices()
    {
    
        return null;
    }
    
    
    /**
     * Obtiene los estudiantes cuyo promedio de quices es mayor o igual a 4
     * @return un vector de objetos de la clase Estudiante
     */
    public Estudiante[] getMayorNotaQuices()
    {
        return null;
    }
    
    /**
     * Obtiene el nombre del quiz que más perdieron los estudiantes (q1, q2....qn)
     * @return un String con el nombre de la columna en Excel
     */
    public String getNombreQuiz_Perdieron()
    {
    
        return "";
    }
    
    
    /**
     * Obtiene la nota que más se repite  (Ojo suponga notas de un entero y un decimal
     * @return un float con la nota que más se repite
     */
    public float getNota_Que_MasRepite()
    {
    
        return 0F;
    }
     
    
    /**
     *  Obtiene un infore de los estudiantes con sus promedios
     * @return una cadena con la información de los estudiantes junto con su promedio
     */
    public String []getPromedio()
    {
        
        //1. Crear el vector resultante:
        String resultado[]=new String[this.listaEstructuras.length];
        //2. Cuando voy a registar elementos de un arreglo utilice for con índice
        for(int i=0;i<resultado.length;i++)
        {
            resultado[i]="Nombre:"+this.listaEstructuras[i].getNombre()+", Promedio:"+this.listaEstructuras[i].getPromedio();
        }
    
        return resultado;
    }
    
    
    public void crearPDF_Promedios() throws Exception
    {
    String resultado[]=this.getPromedio();
    //1. Crear el objeto para manejar el documento:
    Document documento = new Document();
    //2. Crear el objeto para manejar el archivo
    FileOutputStream ficheroPdf = new FileOutputStream("src/Datos/ficheroSalida.pdf");
    //2. Asociamos el fichero al documento:
    PdfWriter.getInstance(documento,ficheroPdf);
    //3. Abrimos el documento:
    documento.open();
    //Crear un parrafo:
     Paragraph parrafo = new Paragraph();
     parrafo.add("Esté es el informe de los estudiantes con sus promedios");
     documento.add(parrafo);
    
     //El constructor recibe la cantidad de columnas
     PdfPTable tabla=new PdfPTable(1);
     boolean color=true;
     for(String datoPromedio:resultado)
     {
         //Celda:
          PdfPCell celda=new PdfPCell(new Phrase(datoPromedio));
          if(color)
                celda.setBackgroundColor(BaseColor.DARK_GRAY);
          else
                celda.setBackgroundColor(BaseColor.WHITE);
          
          color=!color;
          
          tabla.addCell(celda);
         
     }
     
     documento.add(tabla);
    
    
    
    documento.close();
    }
    
}
